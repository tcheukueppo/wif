#!/usr/bin/env python

import re
import sys
import nmcli

nmcli.disable_use_sudo()

def die(error_msg):
    print(error_msg, file = sys.stderr)
    exit(1)

def get(ps1, l):
    try:
        choice = int(input('{0}>> '.format(ps1)))
        if choice < 0 or choice >= l:
            die("Invalid option")
        return choice
    except ValueError:
        die("Invalid option")

try:
    nmcli.general.status()
except NetworkManagerNotRunningException:
    die("NetworkManager isn't running, run `sudo systemctl start NetworkManager'")
except Exception:
    die("Unexpected error")

if wlans := list( filter(lambda x: re.match('wifi', x.device_type), nmcli.device()) ):
    ifname = wlans[0].device

    if len(wlans) > 1:
        print("Choose the Wlan interface you wish to use")
        for i, wlan in enumerate(wlans):
            print("({0}) {1}".format(i, wlan.device))
        ifname = wlans[get('wlan', len(wlans))].device

    apns = nmcli.device.wifi(ifname, True)
    if not apns:
        die("No access points.")

    print("Choose the APN you wish to connect to")
    for i, apn in enumerate(apns):
        print("({0}) {1}".format(i, apn.ssid))

    ssid = apns[get('apn', len(apns))].ssid
    passwd = input('password>> ')
    try:
        nmcli.device.wifi_connect(ssid, passwd, ifname)
    except Exception:
        die("Failed to connect to '{0}'".format(ssid))
else:
    die("No Wlan interface")
